
module.exports = function (WIDTH, HEIGHT) {
  const save = require('./save') // read/write save file
  const grid = require('./grid')(WIDTH, HEIGHT) // grid get/edit

  return {
    grid,
    save
  }
}
