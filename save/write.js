
const fs = require('fs');

module.exports = async function (grid) {
  return new Promise(function(resolve, reject) {
    fs.writeFile('save.json', JSON.stringify(grid), (err) => {
      if (err) throw err
      console.log('[SAVE] Written!');
      resolve()
    })
  })
}
