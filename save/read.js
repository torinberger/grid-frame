
const fs = require('fs');

module.exports = async function () {
  return new Promise(function(resolve, reject) {
    fs.readFile('save.json', (err, data) => {
      if (err) throw err
      console.log('[SAVE] Read!');
      resolve(JSON.parse(data))
    })
  })
}
