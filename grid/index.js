
module.exports = function (WIDTH, HEIGHT) {
  var grid = require('./init')(WIDTH, HEIGHT)

  return {
    async get(x, y) {
      return new Promise(function(resolve, reject) {
        if (grid[x] ? grid[x][y] : false) resolve(grid[x][y])
        else reject()
      })
    },
    async getAll() {
      return new Promise(function(resolve, reject) {
        if (grid) resolve(grid)
        else reject()
      })
    },
    async set(x, y, z) {
      return new Promise(function(resolve, reject) {
        if (grid[x] ? grid[x][y] : false) { grid[x][y] = z; resolve() }
        else reject()
      })
    }
  }
}
