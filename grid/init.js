
module.exports = function (WIDTH, HEIGHT) {
  console.log('[GRID] Creating grid with', WIDTH, 'x', HEIGHT, '...')

  var grid = []

  for (var i = 0; i < WIDTH; i++) {
    grid.push([])
    for (var n = 0; n < HEIGHT; n++) {
      grid[i].push({_x: i, _y: n})
    }
  }

  console.log('[GRID] Done!')

  return grid
}
