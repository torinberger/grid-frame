# grid-frame
This is the base framework for the grid.
## Usage
```javascript
const framework = require('grid-frame')(gridWidth, gridHeight)
```
## Framework
Initialising the grid framework will return an object of the following format.
### .grid
Private `grid` is a two dimensional array containing objects. Each object by default contains `{_x: x, _y: y}`.
#### .get(x, y)
Returns promise. Resolves value of `grid[x][y]` and rejects if `grid[x][y]` is undefined.
#### .getAll()
Returns promise. Resolves value of two dimensional array `grid` and rejects if `grid` is undefined.
#### .set(x, y, z)
Returns promise. Resolves with no value and rejects if `grid[x][y]` is undefined.
### .save
The read/write mechanism for storing grid in JSON format. JSON is stored in `save.json` within the grid-frame directory.
#### .read()
Returns promise. Resolves with value of `grid` and rejects error.
#### .write(grid)
Returns promise. Resolves with no value and rejects error.
